package baseline;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import setup.SetUp;

/*
 * Although Chrome is the only supported browser on Lawyerport,
 *  we have decided to use FireFox for testing purposes due to Firefox being the only browser natively supported by Selenium.
 */

/*
 *     TO DO 
 *     
 * Add all test cases
 * Move password to SQA drive
 * Clean up code
 * Move all test cases out of main arg and use method calls
 * implement loop on main arg
 *
 */
public class BaseLine extends SetUp{
	public static long start;
	public static long end;
	public static long totalTime;
	public long totalTimeAvg = 0;
	public static  WebDriver driver = new FirefoxDriver();
	public static List<String> urlList;
	public static List<Integer> runVars;
	

	//Goes to directory, reads file and extracts url to arraylist urlList
	public static List<String> getUrlsFromFile(String inDir) throws IOException{
		List<String> list = new ArrayList<String>();
		if (inDir == null) {
			return null;
		}
		File dir = new File(inDir);
        File[] files = dir.listFiles();
        for (File f : files) {
            if(f.isFile()) {
                BufferedReader inputStream = null;
                try {
                    inputStream = new BufferedReader(new FileReader(f));
                    String line;
                    while ((line = inputStream.readLine()) != null) {
                    	if (line.contains(".com")) list.add(line);
                    }
                } finally {
                    if (inputStream != null) inputStream.close();
                }
            }
        }
        return list;
	}
	
	//Loads a URL to the driver and adds the total time in ms to the times parameter passed in.
	public static void loadURL(String url, ArrayList<Long> times){
		start = System.currentTimeMillis();
		driver.get(url);
		try{
			  assertEquals("Lawyerport", driver.getTitle());
			  System.out.println("Navigated to LawyerPort");
			}
			catch(Throwable pageNavigationError){
			  System.out.println("Didn't navigate to LawyerPort");
			}
		
		end = System.currentTimeMillis();
		totalTime = end - start;
		times.add(totalTime);
		System.out.println("Load time: " +totalTime+ "ms");
	}

	
	public static void runtTests() throws IOException, InterruptedException{
		String baseUrl = SetUp.url;
		String username = "elptester7500@lawyerport.com";
		String password = "Testpassword1!!";
		
		/*  
		 * LOGGING IN 
		 */
	
		
		System.out.println("Logging in..");
	
		driver.get(baseUrl);
		Thread.sleep(1000);
		driver.findElement(By.id("signin-button")).click();
		driver.findElement(By.id("username")).sendKeys(username);
		driver.findElement(By.id("password")).sendKeys(password);
		driver.findElement(By.id("clientCode")).sendKeys("Selenium");
		start = System.currentTimeMillis();
		driver.findElement(By.id("submitButton")).click();
		
		System.out.println("Successfully logged in as " +username);
		end = System.currentTimeMillis();
		totalTime = end - start;
		System.out.println("Logging in " +totalTime+ "ms");
		
		urlList = getUrlsFromFile("//LB-FS01/42-InfoTech/SQA/AUTOMATED SOFTWARE TESTING/AutomationLawyerPortPropertyFiles/LoadTestUrls/urls");
		
		
		
		ArrayList<Long> loadTimes = new ArrayList<Long>();
		for(String url: urlList){
			loadURL(url, loadTimes);
		}
		
		String fileName = new SimpleDateFormat("yyyy-MM-dd--HHmm'.txt'").format(new Date());
		String dayOfWeek = new SimpleDateFormat("EEEE").format(new Date());
		String year = new SimpleDateFormat("yyyy").format(new Date());
		String month = new SimpleDateFormat("MM").format(new Date());
		String day = new SimpleDateFormat("dd").format(new Date());
		
		String path = "//LB-FS01/42-InfoTech/SQA/AUTOMATED SOFTWARE TESTING/AutomationLawyerPortPropertyFiles/LoadTestUrls/Exported Load Times/"+year+"-"+month+"-"+day+"-" +dayOfWeek+"/" + fileName;
		
		File dirCheck = new File("//LB-FS01/42-InfoTech/SQA/AUTOMATED SOFTWARE TESTING/AutomationLawyerPortPropertyFiles/LoadTestUrls/Exported Load Times/"+year+"-"+month+"-"+day+"-" +dayOfWeek+"/");
		if(!dirCheck.exists()){
			dirCheck.mkdir();
		}
		
		
		File outFile = new File(path);
		PrintWriter writer = new PrintWriter(outFile);
		for(long time: loadTimes){
			String i = String.valueOf(time);
			writer.println(i);
		}
		writer.close();
		
		System.out.println("\n\nFinished... \nTimes exported to "+ fileName +" in milleseconds format");
		System.out.println("Path to file: " + path);
	}
	
	
	public static void main (String [] args) throws IOException, InterruptedException{
		runVars = setRunVarsFromFile("//LB-FS01/42-InfoTech/SQA/AUTOMATED SOFTWARE TESTING/AutomationLawyerPortPropertyFiles/LoadTestUrls/Run Variables");
		
		for(int i = 0; i<runVars.get(0); i++){
			runtTests();
			signOut();
			driver.get("");
			String hr = new SimpleDateFormat("HH").format(new Date());
			String min = new SimpleDateFormat("mm").format(new Date());
			System.out.println("Finished Run "+ (i+1) + " of " + runVars.get(0) + " at " + hr + ":" + min);
			if(i+1 != runVars.get(0)){
				System.out.println(runVars.get(1) + " minutes until next run.");
				Thread.sleep(runVars.get(1)*60*1000);
			}
		}
	}

	private static void signOut() throws InterruptedException {
		try {
			driver.get(SetUp.url);
			Thread.sleep(10000);
		//driver.
			driver.findElement(By.id("button-welcome-link")).click();
			driver.findElement(By.id("my-profile-logout")).click();
			
		}
		catch(Exception e){
			System.out.println("Took too long to load, signing out again");
			signOut();
		}
		
	}
}