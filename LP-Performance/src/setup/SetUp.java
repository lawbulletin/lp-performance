package setup;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.File;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;

import setup.SetUp;

public class SetUp {
	public static String username; //read in username and password
    public static String passwrd;
	public static String targetDir;
	
	/** PICK AN ENVIRONMENT:
	 * Choose which you want to test in, by uncommenting it, and commenting out the other 2.
	 */
	//public static String url = "https://www.lawyerport.com/";   //regular lawyerport
	public static String url = "https://www.lawyerportaws.com/";  //aws lawyerport
	public String baseUrl = url;

	
	public static List<Integer> setRunVarsFromFile(String inDir) throws IOException{
		List<Integer> flist= new ArrayList<Integer>(Arrays.asList(1,1));
		File dir = new File(inDir);
        
		File[] files = dir.listFiles();
        for (File f : files) {
            if(f.isFile()) {
                BufferedReader inputStream = null;
                try {
                    inputStream = new BufferedReader(new FileReader(f));
                    String line;
                    while ((line = inputStream.readLine()) != null) {
                    	if (line.contains("NumberOfRuns="))  flist.set(0, Integer.parseInt(line.substring(13)) );			//First index of List is number of times to loop
                    	if (line.contains("MinutesBetweenEachRun=")) flist.set(1, Integer.parseInt(line.substring(22)));	//Second index of List is Delay between each run in minutes
                    }
                } finally {
                    if (inputStream != null) inputStream.close();
                }
            }
        }
		
		
		System.out.println("Looping "+ flist.get(0) + " times through tests, Delayed by " + flist.get(1) + " minutes each");
		return flist;
	}
	
	public void login(String login, String password, WebDriver driver) throws Exception {
		//WebDriver driver = new FirefoxDriver();
		System.out.println("Logging Into LawyerPort");
		driver.get(baseUrl);
		
		System.out.println("LOGIN: " + login);
		Thread.sleep(1000);
		
		driver.findElement(By.id("signin-button")).click();;
		driver.findElement(By.id("username")).clear();
		driver.findElement(By.id("username")).sendKeys(login);
		driver.findElement(By.id("password")).sendKeys(password);
		driver.findElement(By.id("clientCode")).sendKeys("0");
		driver.findElement(By.id("submitButton")).click();
		//driver.findElement(By.name("submit")).click();


		System.out.println("Determining whether login successful...");
		if (existsElement("password")) { System.out.println("\tLogin Not Successful."); assertTrue(false); }
		else if (existsElement("dashboard")) { System.out.println("\tLogin Successful."); assertTrue (true); }
		else { System.out.println("\tSomething went wrong. Cannot find proper elements."); assertTrue(false); }
	}
	public static String getDir(String url) {
		if (url == "https://www.lawyerport.com/") {
			targetDir =  "//LB-FS01/42-InfoTech/SQA/AUTOMATED SOFTWARE TESTING/AutomationLawyerPortPropertyFiles/AutomationLoginCredentials";
		}
		else if(url == "https://www.lawyerportaws.com/" ){
			targetDir = "//LB-FS01/42-InfoTech/SQA/AUTOMATED SOFTWARE TESTING/AutomationLawyerPortPropertyFiles/AutomationLoginCredentials";
		}
		else {
			targetDir = null;
		}
		return targetDir;
	}
	
	protected boolean existsElement(String id) {
	    try {
	        driver.findElement(By.id(id));
	    } catch (NoSuchElementException e) {
	        return false;
	    }
	    return true;
	}
	protected boolean existsElementbyCSS(String id) {
		try {
			driver.findElement(By.cssSelector(id));
		} catch (NoSuchElementException e) {
			return false;
		}
		return true;
	}
	
	public static String getLogin(String dirFromGetDirMethod) throws IOException {
		if (dirFromGetDirMethod == null) {
			return null;
		}
		File dir = new File(dirFromGetDirMethod);
        File[] files = dir.listFiles();
        for (File f : files) {
            if(f.isFile()) {
                BufferedReader inputStream = null;
                try {
                    inputStream = new BufferedReader(new FileReader(f));
                    String line;
                    while ((line = inputStream.readLine()) != null) {
                    	if (line.contains("username=")) return line.substring(9);
                    }
                } finally {
                    if (inputStream != null) inputStream.close();
                }
            }
        }
        return null;
       }
	
	public static String getPassword(String dirFromGetDirMethod) throws IOException {
		if (dirFromGetDirMethod == null) {
			return null;
		}
		File dir = new File(dirFromGetDirMethod);
        File[] files = dir.listFiles();
        for (File f : files) {
            if(f.isFile()) {
                BufferedReader inputStream = null;
                try {
                    inputStream = new BufferedReader(new FileReader(f));
                    String line;
                    while ((line = inputStream.readLine()) != null) {
                    	if (line.contains("password=")) return line.substring(9);
                    }
                } catch (IOException e){
                    System.out.println("createDirectory failed:" + e);
                } finally {
                    if (inputStream != null) inputStream.close();
                }
            }
        }
        return null;
       }

		WebDriver driver;
		int saving_tries = 1;


}
